package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"golang.org/x/net/html"
)

// Finds the href attribute and returns its value
func findHref(n html.Token) (ok bool, href string) {
	for _, a := range n.Attr {
		if a.Key == "href" {
			href = a.Val
			ok = true
		}
	}
	return
}

// Returns a link
func findAnchor(n html.Token) (link Link) {
	if n.Data == "a" {
		_, href := findHref(n)
		ext := hasProtocol(href)
		link = Link{ext, href, false}
	}
	return
}

// Check whether a url is prepended with http
func hasProtocol(url string) bool {
	return strings.Index(url, "http") == 0
}

// Parses entire HTML page, returning all links
func crawl(url string, chURLS chan Link, chDone chan bool) (links []Link) {
	defer func() {
		chDone <- true
	}()

	resp, err := http.Get(url)
	if err != nil {
		log.Fatal(err)
	}

	n := html.NewTokenizer(resp.Body)

	defer resp.Body.Close()

	for {
		tokenT := n.Next()

		switch {
		case tokenT == html.ErrorToken:
			return
		case tokenT == html.StartTagToken:
			token := n.Token()

			anchor := findAnchor(token)
			if anchor.address == "" {
				break
			}

			if !hasProtocol(anchor.address) {
				chURLS <- anchor
			}
		}
	}
}

// Link Represents a link on a webpage
type Link struct {
	external bool
	address  string
	root     bool
}

// Captures all links, passes to itself
func crawlLoop(depth int, currentDepth int, urls []Link, rootURL Link) {
	chURLS := make(chan Link)
	chDone := make(chan bool)

	var newUrls []Link

	if urls == nil {
		urls = append(urls, rootURL)
	}

	for _, url := range urls {
		fmt.Println("Running", len(urls), "different urls")
		if !url.root {
			go crawl(rootURL.address+url.address, chURLS, chDone)
		} else {
			go crawl(url.address, chURLS, chDone)
		}
	}

	// Collect all urls in newUrls
	for c := 0; c < len(urls); {
		select {
		case url := <-chURLS:
			fmt.Println(url)
			newUrls = append(newUrls, url)
		case <-chDone:
			c++
		}
	}

	if currentDepth < depth {
		urls = newUrls
		currentDepth++
		crawlLoop(depth, currentDepth, urls, rootURL)
	}
}

func main() {
	root := Link{false, "https://about.gitlab.com", true}
	var urls []Link

	depth, _ := strconv.Atoi(os.Args[1])
	crawlLoop(depth, 1, urls, root)
}
